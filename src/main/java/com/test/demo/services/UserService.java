package com.test.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import com.test.demo.repository.UserRepository;
import com.test.demo.user.UserEntity;

import javax.validation.Valid;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserEntity create(@Valid UserEntity userEntity) {
        return userRepository.save(userEntity);
    }

    public UserEntity findById(Long id) {
        return userRepository.findById(id).orElse(null);
    }

    public Page<UserEntity> findAll(Specification<UserEntity> filteringSpecification, Pageable paging) {
        return userRepository.findAll(filteringSpecification, paging);
    }

    public void updateUser(@Valid UserEntity userEntity) {
        userRepository.save(userEntity);
    }
}
