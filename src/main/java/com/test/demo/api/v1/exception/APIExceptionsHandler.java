package com.test.demo.api.v1.exception;

import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;

import org.slf4j.Logger;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

@ControllerAdvice
public class APIExceptionsHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(APIExceptionsHandler.class);

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<ExceptionsMessageWrapper> handleUnsupportedMethods(
            HttpRequestMethodNotSupportedException ex) {
        ExceptionsMessageWrapper message = new ExceptionsMessageWrapper(ex.getLocalizedMessage());

        return new ResponseEntity<>(message, HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ExceptionsMessageWrapper> handleMessageNotReadable(
            HttpMessageNotReadableException ex) {
        ExceptionsMessageWrapper message = new ExceptionsMessageWrapper("JSON syntax error " + ex.getCause().getMessage());

        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<ExceptionsMessageWrapper> handleAccessDeniedException (AccessDeniedException ex) {

        ExceptionsMessageWrapper message = new ExceptionsMessageWrapper(ex.getMessage());

        return new ResponseEntity<>(message, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ExceptionsMessageWrapper> handleMethodArgumentNotValidException(
            MethodArgumentNotValidException ex) {

        String message = ex.getBindingResult().getAllErrors()
                .stream().map( this :: getMessageFromObjectError )
                .collect( Collectors.joining( ", " ) );

        ExceptionsMessageWrapper messageWrapper = new ExceptionsMessageWrapper( message );

        return new ResponseEntity<>(messageWrapper, HttpStatus.BAD_REQUEST);

    }

    private String getMessageFromObjectError( ObjectError objectError ) {
        StringBuilder stringBuilder = new StringBuilder(50);
        stringBuilder.append("'");
        if( objectError instanceof FieldError) {
            FieldError fieldError = ( FieldError ) objectError;
            stringBuilder.append(fieldError.getField());
        } else {
            stringBuilder.append(objectError.getObjectName());
        }
        stringBuilder.append("' ").append(objectError.getDefaultMessage());
        return stringBuilder.toString();
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ExceptionsMessageWrapper> handleConstraintViolationException(
            ConstraintViolationException ex) {

        String message = ex.getConstraintViolations()
                .stream().map( ConstraintViolation:: getMessage )
                .collect( Collectors.joining( ", " ) );

        ExceptionsMessageWrapper messageWrapper = new ExceptionsMessageWrapper( message );

        return new ResponseEntity<>(messageWrapper, HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler(ResponseStatusException.class)
    public ResponseEntity<ExceptionsMessageWrapper> handleBadRequests(ResponseStatusException ex) {

        ExceptionsMessageWrapper messageWrapper = new ExceptionsMessageWrapper(ex.getReason());

        return new ResponseEntity<>(messageWrapper, ex.getStatus());
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ExceptionsMessageWrapper> handleMethodArgumentTypeMismatch(
            MethodArgumentTypeMismatchException ex) {

        String message = ex.getRequiredType() == null ?
                "Bad request"
                : "Path variable must be of type " + ex.getRequiredType().getSimpleName();
        ExceptionsMessageWrapper messageWrapper = new ExceptionsMessageWrapper(message);

        return new ResponseEntity<>(messageWrapper, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity<ExceptionsMessageWrapper> handleBadCredentialsException(BadCredentialsException ex) {
        ExceptionsMessageWrapper messageWrapper = new ExceptionsMessageWrapper(ex.getMessage());

        return new ResponseEntity<>(messageWrapper, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<ExceptionsMessageWrapper> handleUsernameNotFoundException(UsernameNotFoundException ex) {
        ExceptionsMessageWrapper messageWrapper = new ExceptionsMessageWrapper(ex.getMessage());

        return new ResponseEntity<>(messageWrapper, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ExceptionsMessageWrapper> handleInternalExceptions(RuntimeException ex) {

        LOGGER.error( "Internal Server Error",  ex);
        ExceptionsMessageWrapper message = new ExceptionsMessageWrapper("Internal Server Error");

        return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}