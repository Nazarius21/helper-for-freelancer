# Helper For Freelancer

При створенні проєкту використано мову Java версії 8,
Для збірки проєкту використано Maven


**Для роботи вам потрібно**

В якості середовища розробки може бути використано - Eclipse, IntelliJ IDEA
СУБД: H2, PostgreSQL 12
Для тестування: Postman, Swagger

**Конфігурація БД:**

* Для розробки: H2
* Для продакшна: PostgreSQL 12

Для того, щоб база даних не перетиралася при закритті потрібно у файлі application-dev.properties рядок  
spring.datasource.url=jdbc:h2:file:./maindb;MODE=PostgreSQL  
замінити на наступний  
spring.datasource.url=jdbc:h2:file:./maindb;MODE=PostgreSQL;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE;AUTO_RECONNECT=TRUE

**Опис файлів конфігурації та створення нових**

>*Файли конфігурації:*
>* application-dev.properties - тут знаходиться конфігурація бази даних для development (знаходиться /src/main/resources/application-dev.properties);
>* application-prod.properties - тут знаходиться конфігурація бази даних для продакшену (знаходиться /src/main/resources/application-prod.properties);

**Запуск проекта:**

Для запуску проєкта Вам необхідно відкрити проєкт в середовищі розробки Eclipse або IntelliJ IDEA.

Команда для запуску

Eclipse ->

Права кнопка мишки над проектом -> Run As -> Maven build...

Goals: clean spring-boot:run

Profiles: вписуєте потрібну назву профілю

Run

Команда для запуску

IntelliJ IDEA ->

Натиснути на кнопку з правої сторони меню "Maven",

Після чого натиснути на кнопку "Run Maven Build"

Terminal ->
mvn clean spring-boot:run -Pyour_profile_name ->

# H2 develop mode
mvn clean spring-boot:run -Pdev
__________________________________________
# Postgresql develop mode
mvn clean spring-boot:run -Pdev-postgresql
__________________________________________
# Postgresql production mode
mvn clean spring-boot:run -Pprod
__________________________________________

При перевірці запитів в Postman в Headers треба вказати такі дані: Key: Content-Type, Value: application/json.

Для запуску тестування, вам потрібно запустити JUnit тести, котрі знаходяться в пакеті /test/java/com.test.demo/.

Оскільки проект створюється в різних IDE, потрібно задати певні **налаштування середовища розробки**, щоб не виникало розбіжностей при Merge Requests.

**Для Eclipse:**

Window/Preferences -> General | Workspace:

Text file encoding -> Other: UTF-8

New text file line delimiter -> Default (Windows)


**Для IntelliJ IDEA:**

Settings/Editor -> File Encodings -> Global Encoding, Project Encoding: UTF-8

Settings/Editor -> Code Style -> General: Line separator -> Windows (\r\n);

або задати ці параметри в рядку стану.

**Для зміни налаштувань git**

Команда $ git config core.autocrlf використовується для зміни способу обробки Git закінчень рядків.

Якщо її результатом є true, нічого змінювати не потрібно, інакше треба використати команду

$ git config --local core.autocrlf true

в каталозі проекту.

Щоб нормалізувати закінчення рядків

$ git add --renormalize .

Подивитися нормалізовані файли

$ git status

Закомітити зміни в репозиторій

$ git commit -m "Normalize all the line endings"