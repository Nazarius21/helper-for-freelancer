package com.test.demo.api.filtering.predicate;

import com.test.demo.api.searching.SearchCriteria;
import org.springframework.data.jpa.domain.Specification;

public interface SpecificationBuilder<T> {

     Specification <T> buildSpecification(SearchCriteria searchCriteria);

}