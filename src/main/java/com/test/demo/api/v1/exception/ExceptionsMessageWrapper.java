package com.test.demo.api.v1.exception;

public class ExceptionsMessageWrapper {

    private String reason;

    public ExceptionsMessageWrapper(){
    }

    public ExceptionsMessageWrapper(String message) {
        this.reason = message;
    }

    public String getReason() {
        return reason;
    }
}
