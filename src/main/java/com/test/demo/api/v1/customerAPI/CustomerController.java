package com.test.demo.api.v1.customerAPI;

import com.test.demo.api.ServiceAPIUrl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = CustomerController.BASE_URL)
public class CustomerController {

    public static final String BASE_URL = ServiceAPIUrl.V1_PATH + "/customer";

    @Autowired
    private CustomerAPIService customerConvertingService;

    @ApiOperation(value = "create new customer", authorizations = { @Authorization(value="JWT") })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public Customer create(@Valid @RequestBody Customer customer) {
        return customerConvertingService.create(customer);
    }

    @ApiOperation(value = "View a customer by specific id")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Customer getUser(@ApiParam(value = "Identification number of customer", example = "1") @PathVariable Long id) {
        return customerConvertingService.getCustomer(id);
    }

    // find all customer

    @ApiOperation(value = "Update a customer by specific id", authorizations = { @Authorization(value="JWT") })
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void editCustomer(@ApiParam(value = "Identification number of customer", example = "1") @PathVariable Long id,
                         @Valid @RequestBody Customer customerRequest) {
        customerConvertingService.editCustomer(id, customerRequest);
    }
}