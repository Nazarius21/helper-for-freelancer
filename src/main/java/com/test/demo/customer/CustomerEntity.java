package com.test.demo.customer;

import com.test.demo.user.UserEntity;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "customers")
public class CustomerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "first_name", nullable = false)
    @NotBlank
    private String firstname;

    @Column(name = "last_name", nullable = false)
    @NotBlank
    private String lastname;

    @Column(name = "phone")
    private String phone;

    @Column(name = "telegram_nickname")
    private String telegram_nickname;

    public CustomerEntity() {
    }

    public CustomerEntity(@NotBlank String firstname, @NotBlank String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public CustomerEntity(@NotBlank String firstname, @NotBlank String lastname, String phone, String telegram_nickname) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.phone = phone;
        this.telegram_nickname = telegram_nickname;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTelegram_nickname() {
        return telegram_nickname;
    }

    public void setTelegram_nickname(String telegram_nickname) {
        this.telegram_nickname = telegram_nickname;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof CustomerEntity)) {
            return false;
        }
        if(this == obj) {
            return true;
        }
        final CustomerEntity otherObject = (CustomerEntity) obj;

        return new EqualsBuilder()
                .append(this.id, otherObject.id)
                .append(this.firstname, otherObject.firstname)
                .append(this.lastname, otherObject.lastname)
                .append(this.phone, otherObject.phone)
                .append(this.telegram_nickname, otherObject.telegram_nickname)
                .isEquals();
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(getId());
        builder.append(getFirstname());
        builder.append(getLastname());
        builder.append(getPhone());
        builder.append(getTelegram_nickname());
        return builder.hashCode();
    }

    @Override
    public String toString() {
        return "CustomerEntity{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", phone='" + phone + '\'' +
                ", telegram_nickname='" + telegram_nickname + '\'' +
                '}';
    }
}
