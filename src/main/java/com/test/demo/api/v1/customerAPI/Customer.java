package com.test.demo.api.v1.customerAPI;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;

public class Customer {

    @ApiModelProperty(notes = "The identification unique number of customer", required = true, example = "1")
    private long id;

    @NotBlank
    @ApiModelProperty(notes = "The first name of a customer", required = true, example = "Mike")
    private String firstname;

    @NotBlank
    @ApiModelProperty(notes = "The last name of customer <br> filterable equals (=) and contains(:), this field has been sorted", required = true, example = "Torino")
    private String lastname;

    @ApiModelProperty(notes = "The phone number a customer")
    private String phone;

    @ApiModelProperty(notes = "The customer telegram nickname")
    private String telegram_nickname;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTelegram_nickname() {
        return telegram_nickname;
    }

    public void setTelegram_nickname(String telegram_nickname) {
        this.telegram_nickname = telegram_nickname;
    }
}
