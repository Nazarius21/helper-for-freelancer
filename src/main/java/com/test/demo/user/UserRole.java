package com.test.demo.user;

public enum UserRole {
    ROLE_USER,
    ROLE_ADMIN;
}
