package com.test.demo.api.v1.customerAPI;

import com.test.demo.customer.CustomerEntity;
import com.test.demo.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class CustomerAPIService {

    @Autowired
    private CustomerService customerService;

    public Customer create (Customer customerRequest) {
        CustomerEntity customerEntity = new CustomerEntity();
        copyProperties(customerRequest, customerEntity);

        CustomerEntity createdUser = customerService.create(customerEntity);
        return convertToCustomerDTO(createdUser);
    }

    // find all customer

    public void editCustomer(Long id, Customer customerRequest) {
        CustomerEntity customerEntity = getValidatedCustomer(id);
        copyProperties(customerRequest, customerEntity);
        customerService.updateCustomer(customerEntity);
    }

    public Customer getCustomer(Long id) {
        CustomerEntity customerEntity = getValidatedCustomer(id);
        return convertToCustomerDTO(customerEntity);
    }

    private CustomerEntity getValidatedCustomer(Long id){
        CustomerEntity customerEntity = customerService.findById(id);

        if (customerEntity == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Customer with id " + id + " was not found.");
        }
        return customerEntity;
    }

    private Sort getSortedByDefault(Sort sort) {
        return sort.and(Sort.by(Sort.Direction.ASC, "id"));
    }

    private Customer convertToCustomerDTO(CustomerEntity customerEntity) {
        Customer customer = new Customer();
        customer.setId(customerEntity.getId());
        customer.setFirstname(customerEntity.getFirstname());
        customer.setLastname(customerEntity.getLastname());
        customer.setPhone(customerEntity.getPhone());
        customer.setTelegram_nickname(customerEntity.getTelegram_nickname());
        return customer;
    }

    private void copyProperties(Customer customerRequest, CustomerEntity customer){
        customer.setFirstname(customerRequest.getFirstname());
        customer.setLastname(customerRequest.getLastname());
        customer.setPhone(customerRequest.getPhone());
        customer.setTelegram_nickname(customerRequest.getTelegram_nickname());
    }
}
