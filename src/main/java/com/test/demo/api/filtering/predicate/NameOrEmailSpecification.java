package com.test.demo.api.filtering.predicate;


import com.test.demo.user.UserEntity;
import com.test.demo.api.filtering.FilteringOperation;
import com.test.demo.api.searching.SearchCriteria;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class NameOrEmailSpecification implements Specification<UserEntity> {

    private static final long serialVersionUID = 7860218411885895782L;
    private SearchCriteria searchCriteria;

    public NameOrEmailSpecification(SearchCriteria searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

    @Override
    public Predicate toPredicate(Root<UserEntity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        if (searchCriteria.getValue() != null) {
            List<Predicate> predicates = new ArrayList<>();
            if (FilteringOperation.EQUAL == searchCriteria.getOperation()) {
                predicates.add(cb.equal(root.get("lastname"), searchCriteria.getValue()));
                predicates.add(cb.equal(root.get("email"), searchCriteria.getValue()));
            }
            if (FilteringOperation.CONTAIN == searchCriteria.getOperation()) {
                String searchValueLowerCase = StringUtils.lowerCase((String) searchCriteria.getValue());
                predicates.add(cb.like(cb.lower(root.get("lastname")), "%" + searchValueLowerCase + "%"));
                predicates.add(cb.like(cb.lower(root.get("email")), "%" + searchValueLowerCase + "%"));
            }
            return cb.or(predicates.toArray(new Predicate[predicates.size()]));
        }
        return null;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof NameOrEmailSpecification)) {
            return false;
        }
        if (this == o) {
            return true;
        }
        final NameOrEmailSpecification otherObject = (NameOrEmailSpecification) o;

        return new EqualsBuilder()
                .append(this.searchCriteria, otherObject.searchCriteria)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(searchCriteria)
                .hashCode();
    }
}
