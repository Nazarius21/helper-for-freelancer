package com.test.demo.config;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String DEFAULT_INCLUDE_PATTERN = "/api/.*";

    @Bean
    public Docket swaggerApi10(){
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title(SwaggerInfo.TITLE)
                .description(SwaggerInfo.DESCRIPTION)
                .version(SwaggerInfo.API_VERSION_V1).build();
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(SwaggerInfo.GROUP_NAME + SwaggerInfo.API_VERSION_V1)
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo)
                .select().paths(regex(SwaggerInfo.PATH_V1))
                .build()
                .produces(SwaggerInfo.CONSUMES_AND_PRODUCES)
                .consumes(SwaggerInfo.CONSUMES_AND_PRODUCES)
//                .tags(new Tag(SwaggerInfo.USER_API, SwaggerInfo.USER_CONTROLLER_INFO))
//                .tags(new Tag(SwaggerInfo.NOTIFICATION_API, SwaggerInfo.NOTIFICATION_CONTROLLER_INFO))
                .securityContexts(Lists.newArrayList(securityContext()))
                .securitySchemes(Lists.newArrayList(apiKey()));
    }

    @Bean
    public UiConfiguration uiConfiguration(){
        return UiConfigurationBuilder.builder()
                .deepLinking(true)
                .validatorUrl(null)
                .build();
    }

    private ApiKey apiKey(){
        return new ApiKey("JWT", AUTHORIZATION_HEADER, "header");
    }

    private SecurityContext securityContext(){
        return SecurityContext.builder()
                .securityReferences(defaultAuth())
                .forPaths(PathSelectors.regex(DEFAULT_INCLUDE_PATTERN))
                .build();
    }

    List<SecurityReference> defaultAuth(){
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Lists.newArrayList(new SecurityReference("JWT", authorizationScopes));
    }
}
