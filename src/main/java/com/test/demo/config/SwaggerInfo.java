package com.test.demo.config;

import com.google.common.collect.Sets;
import org.springframework.http.MediaType;

import java.util.Set;

public class SwaggerInfo {

    private SwaggerInfo() {}

//    public static final String USER_API = "User API";

//    public static final String NOTIFICATION_API = "Notification API";

    public static final Set<String> CONSUMES_AND_PRODUCES = Sets.newHashSet(MediaType.APPLICATION_JSON_VALUE);

    public static final String API_VERSION_V1 = "1.0";

    public static final String PATH_V1 = "/v1/.*";

    public static final String DESCRIPTION = "API for fetching user related information";

    public static final String TITLE = "Helper For Freelancers API";

    public static final String GROUP_NAME = "helper-for-freelancers-api-";

    public static final String USER_CONTROLLER_INFO = "The controller for user operations";

    public static final String NOTIFICATION_CONTROLLER_INFO = "The controller for notification operations";
}
