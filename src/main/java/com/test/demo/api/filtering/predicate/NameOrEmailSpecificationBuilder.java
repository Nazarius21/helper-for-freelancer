package com.test.demo.api.filtering.predicate;


import com.test.demo.user.UserEntity;
import com.test.demo.api.filtering.FilteringOperation;
import com.test.demo.api.searching.SearchCriteria;
import org.springframework.data.jpa.domain.Specification;

import java.util.Arrays;
import java.util.List;

public class NameOrEmailSpecificationBuilder implements SpecificationBuilder<UserEntity> {

    public static final List<FilteringOperation> SUPPORTED_OPERATORS = Arrays.asList(
            FilteringOperation.EQUAL, FilteringOperation.CONTAIN);

    @Override
    public Specification<UserEntity> buildSpecification(SearchCriteria searchCriteria) {
        return new NameOrEmailSpecification(searchCriteria);
    }
}