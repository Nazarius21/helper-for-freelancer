package com.test.demo;

import com.test.demo.config.UniqueBeanNameGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@ComponentScan(nameGenerator = UniqueBeanNameGenerator.class)
public class HelperForFreelancers {

	public static void main(String[] args) {
		SpringApplication.run(HelperForFreelancers.class, args);
	}
}
