package com.test.demo.api.v1.userAPI;

import com.test.demo.user.UserRole;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class User {

    @ApiModelProperty(notes = "The identification unique number of user", required = true, example = "1")
    private long id;

    @NotBlank
    @ApiModelProperty(notes = "The first name of a user", required = true, example = "Nazar")
    private String firstname;

    @NotBlank
    @ApiModelProperty(notes = "The last name of user <br> filterable equals (=) and contains(:), this field has been sorted", required = true, example = "Hlukhaniuk")
    private String lastname;

    @Email
    @ApiModelProperty(notes = "The email a user <br> filterable equals (=)  and contains(:), this field has been sorted")
    private String email;

    @ApiModelProperty(notes = "The phone number a user")
    private String phone;

    @ApiModelProperty(notes = "The role of a user", required = true, example = "ROLE_ADMIN")
    private UserRole userRole;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }
}
