package com.test.demo.api.filtering.predicate;

import java.util.Arrays;
import java.util.List;

import com.test.demo.user.UserEntity;
import com.test.demo.api.filtering.EntityFilterSpecificationsBuilder;
import com.test.demo.api.filtering.FilterableProperty;
import org.springframework.stereotype.Component;

@Component
public class UserSpecificationsBuilder implements EntityFilterSpecificationsBuilder<UserEntity> {

    public static final List<FilterableProperty<UserEntity>> FILTERABLE_PROPERTIES = Arrays.asList(
            new FilterableProperty<>("lastname", String.class, NameOrEmailSpecificationBuilder.SUPPORTED_OPERATORS, new NameOrEmailSpecificationBuilder()),
            new FilterableProperty<>("email", String.class, NameOrEmailSpecificationBuilder.SUPPORTED_OPERATORS, new NameOrEmailSpecificationBuilder())
    );

    @Override
    public List<FilterableProperty<UserEntity>> getFilterableProperties() {
        return FILTERABLE_PROPERTIES;
    }
}