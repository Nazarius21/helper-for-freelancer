package com.test.demo.services;

import com.test.demo.customer.CustomerEntity;
import com.test.demo.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.validation.Valid;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public CustomerEntity create(@Valid CustomerEntity customerEntity) {
        return customerRepository.save(customerEntity);
    }

    public CustomerEntity findById(Long id) {
        return customerRepository.findById(id).orElse(null);
    }

    public Page<CustomerEntity> findAll(Specification<CustomerEntity> filteringSpecification, Pageable paging) {
        return customerRepository.findAll(filteringSpecification, paging);
    }

    public void updateCustomer(@Valid CustomerEntity customerEntity) {
        customerRepository.save(customerEntity);
    }
}
