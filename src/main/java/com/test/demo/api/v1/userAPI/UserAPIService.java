package com.test.demo.api.v1.userAPI;

import com.test.demo.user.UserEntity;
import com.test.demo.services.UserService;
import com.test.demo.api.v1.ResponsePage;
import com.test.demo.api.filtering.predicate.UserSpecificationsBuilder;
import com.test.demo.api.searching.SearchCriteria;
import com.test.demo.api.searching.SearchCriteriaParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserAPIService {

    public static final Integer DEFAULT_PAGE_SIZE = 8;
    public static final Integer DEFAULT_PAGE_NUMBER = 0;

    @Autowired
    private UserService userService;

    @Autowired
    private UserSpecificationsBuilder userSpecificationsBuilder;

    @Autowired
    private SearchCriteriaParser searchCriteriaParser;

    public User create (User userRequest) {
        UserEntity userEntity = new UserEntity();
        copyProperties(userRequest, userEntity);

        UserEntity createdUser = userService.create(userEntity);
        return convertToUserDTO(createdUser);
    }

    public void editUser(Long id, User userRequest) {
        UserEntity userEntity = getValidatedUser(id);
        copyProperties(userRequest, userEntity);
        userService.updateUser(userEntity);
    }

    public User getUser(Long id) {
        UserEntity userEntity = getValidatedUser(id);
        return convertToUserDTO(userEntity);
    }

    public ResponsePage<User> get(Optional<String> search, Optional<Integer> pageNo, Optional<Integer> pageSize, Sort sort) {
        Specification<UserEntity> filteringSpecification = null;
        if (search.isPresent()) {
            String search1 = search.get();
            List<SearchCriteria> searchCriteria = searchCriteriaParser.parseSearchCriteria(search1, userSpecificationsBuilder.getFilterableProperties());
            filteringSpecification = userSpecificationsBuilder.buildSpecification(searchCriteria);
        }
        return getResponsePage(
                filteringSpecification,
                pageNo.orElse(DEFAULT_PAGE_NUMBER),
                pageSize.orElse(DEFAULT_PAGE_SIZE), sort);
    }


    public ResponsePage< User > getResponsePage(Specification<UserEntity> filteringSpecification, Integer pageNo, Integer pageSize, Sort sort){
        sort = getSortedByDefault(sort);
        Pageable paging = PageRequest.of(pageNo, pageSize, sort);
        Page<UserEntity> pagedResult = userService.findAll(filteringSpecification, paging);
        List< User > users = pagedResult.getContent()
                .stream().map(this::convertToUserDTO).collect(Collectors.toList());
        return new ResponsePage<>(users, pagedResult.getTotalElements());
    }

    private UserEntity getValidatedUser(Long id){
        UserEntity userEntity = userService.findById(id);

        if (userEntity == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User with id " + id + " was not found.");
        }
        return userEntity;
    }

    private Sort getSortedByDefault(Sort sort) {
        return sort.and(Sort.by(Sort.Direction.ASC, "id"));
    }

    private User convertToUserDTO(UserEntity userEntity) {
        User user = new User();
        user.setId(userEntity.getId());
        user.setFirstname(userEntity.getFirstName());
        user.setLastname(userEntity.getLastName());
        user.setEmail(userEntity.getEmail());
        user.setPhone(userEntity.getPhone());
        user.setUserRole(userEntity.getUserRole());
        return user;
    }

    private void copyProperties(User userRequest, UserEntity user){
        user.setFirstName(userRequest.getFirstname());
        user.setLastName(userRequest.getLastname());
        user.setEmail(userRequest.getEmail());
        user.setPhone(userRequest.getPhone());
        user.setUserRole(userRequest.getUserRole());
    }
}
