//package com.test.demo.user;
//
//import com.test.demo.repository.UserRepository;
//import com.test.demo.services.UserService;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.security.core.context.SecurityContextHolder;
//import org.springframework.test.annotation.DirtiesContext;
//
//import javax.validation.ConstraintViolationException;
//
//import static org.junit.jupiter.api.Assertions.assertTrue;
//import static org.junit.jupiter.api.Assertions.fail;
//
//@ExtendWith(MockitoExtension.class)
//@DirtiesContext
//public class UserServiceTest {
//
//    @MockBean
//    private UserRepository userRepository;
//
//    @Autowired
//    private UserService userService;
//
//    @BeforeEach
//    public void setup() {
//        SecurityContextHolder.clearContext();
//    }
//
//    @Test
//    void testNameNotNull() {
//        UserEntity userEntity = getValidUserEntity();
//        userEntity.setFirstName(null);
//        testConstrainViolationForCreating(userEntity, "firstName");
//    }
//
//    @Test
//    void testEmailNotNull() {
//        UserEntity userEntity = getValidUserEntity();
//        userEntity.setEmail("asdasdsa");
//        testConstrainViolationForCreating(userEntity, "email");
//    }
//    @Test
//    void testLastNameNotNull() {
//        UserEntity userEntity = getValidUserEntity();
//        userEntity.setLastName(null);
//        testConstrainViolationForCreating(userEntity, "lastName");
//    }
//
//    @Test
//    void testLastNameNotBlank() {
//        UserEntity userEntity = getValidUserEntity();
//        userEntity.setLastName("");
//        testConstrainViolationForCreating(userEntity, "lastName");
//    }
//
//    @Test // expected no exception
//    void validationOK() {
//        UserEntity userEntity = getValidUserEntity();
//        userService.create(userEntity);
//    }
//
//    @Test
//    void testLastNameManySpaces() {
//        UserEntity userEntity = getValidUserEntity();
//        userEntity.setLastName("                                          ");
//        testConstrainViolationForCreating(userEntity, "lastName");
//    }
//
//    @Test
//    void testIsValidatingRunning() {
//        UserEntity userEntity = getValidUserEntity();
//        userEntity.setLastName("                                          ");
//        testConstrainViolationForEditing(userEntity, "lastName");
//    }
//
//    private void testConstrainViolationForCreating(UserEntity userEntity, String validationCheck) {
//        try {
//            userService.create(userEntity);
//        } catch (ConstraintViolationException e) {
//            assertTrue(e.getMessage().contains(validationCheck));
//            return;
//        }
//        fail();
//    }
//
//    private void testConstrainViolationForEditing(UserEntity userEntity, String validationCheck) {
//        try {
//            userService.updateUser(userEntity);
//        } catch (ConstraintViolationException e) {
//            assertTrue(e.getMessage().contains(validationCheck));
//            return;
//        }
//        fail();
//    }
//
//    private UserEntity getValidUserEntity() {
//        UserEntity userEntity = new UserEntity();
//        userEntity.setId(1L);
//        userEntity.setFirstName("Nazar");
//        userEntity.setLastName("Hlukhaniuk");
//        userEntity.setEmail("hlukhanyuk.nazar@gmail.com");
//        userEntity.setPhone(996372898L);
//        userEntity.setUserRole(UserRole.ROLE_ADMIN);
//        return userEntity;
//    }
//
//    private UserEntity getUserEntity(Long id, String firstname, String lastname, String email, Long phone, UserRole userRole) {
//        UserEntity user = new UserEntity();
//        user.setId(id);
//        user.setFirstName(firstname);
//        user.setLastName(lastname);
//        user.setEmail(email);
//        user.setPhone(phone);
//        user.setUserRole(userRole);
//        return user;
//    }
//}
