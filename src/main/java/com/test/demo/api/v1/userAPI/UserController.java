package com.test.demo.api.v1.userAPI;

import com.test.demo.api.ServiceAPIUrl;
import com.test.demo.api.v1.ResponsePage;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping(value = UserController.BASE_URL)
public class UserController {

    public static final String BASE_URL = ServiceAPIUrl.V1_PATH + "/user";

    @Autowired
    private UserAPIService userConvertingService;

    @ApiOperation(value = "create new user", authorizations = { @Authorization(value="JWT") })
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public User create(@Valid @RequestBody User user) {
        return userConvertingService.create(user);
    }

    @ApiOperation(value = "View a list of all users")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponsePage<User> getAllUsers(
            @ApiParam(name = "search", value = "Search expression used to filter results. To check fields which support filtering, please refer to response model")
            @RequestParam(value = "search", required = false) Optional<String> search,
            @ApiParam(name = "pageNo", value = "Search expression used to page number (pageNo). To check fields which support page default value, please refer to response model")
            @RequestParam(required = false) Optional<Integer> pageNo,
            @ApiParam(name = "pageSize", value = "Search expression used to page size value (pageSize). To check fields which support page size value, please refer to response model")
            @RequestParam(required = false) Optional<Integer> pageSize,
            final @ApiParam(name = "sort", value = "Specify sort fields") Sort sort){
        return userConvertingService.get(search, pageNo, pageSize, sort);
    }

    @ApiOperation(value = "View a user by specific id")
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public User getUser(@ApiParam(value = "Identification number of user", example = "1") @PathVariable Long id) {
        return userConvertingService.getUser(id);
    }

    @ApiOperation(value = "Update a user by specific id", authorizations = { @Authorization(value="JWT") })
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void editUser(@ApiParam(value = "Identification number of user", example = "1") @PathVariable Long id,
                         @Valid @RequestBody User userRequest) {
        userConvertingService.editUser(id, userRequest);
    }
}
