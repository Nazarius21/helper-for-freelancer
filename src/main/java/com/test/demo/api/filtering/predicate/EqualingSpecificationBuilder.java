package com.test.demo.api.filtering.predicate;

import java.util.Arrays;
import java.util.List;

import com.test.demo.api.filtering.FilteringOperation;
import com.test.demo.api.searching.SearchCriteria;
import org.springframework.data.jpa.domain.Specification;

public class EqualingSpecificationBuilder<EntityType> implements SpecificationBuilder<EntityType> {

    public static final List<FilteringOperation> SUPPORTED_OPERATORS = Arrays.asList(
            FilteringOperation.EQUAL, FilteringOperation.NOT_EQUAL
    );

    @Override
    public Specification<EntityType> buildSpecification(SearchCriteria searchCriteria) {
        return new EqualingSpecification<>(searchCriteria);
    }
}